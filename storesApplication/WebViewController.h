//
//  WebViewController.h
//  storesApplication
//
//  Created by vendor on 21/2/15.
//  Copyright (c) 2015 shblimted. All rights reserved.
//

//--------------------------------------------------------------------------------------------------
//  0    1         2         3         4         5         6         7         8         9
//  567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890

#import <UIKit/UIKit.h>

#define WEBVIEW_SINGLE_SCANNER                                      @"singleScanner"
#define WEBVIEW_MULTIPLE_SCANNER                                    @"multipleScanner"



@class AppManager;

//--------------------------------------------------------------------------------------------------
@interface WebViewController : UIViewController <UIWebViewDelegate>  {
    UIWebView *_webView;
    NSString *_urlString;
    
    AppManager *_appManager;
}

//--------------------------------------------------------------------------------------------------
@property (nonatomic, retain) UIWebView *webView;
@property (nonatomic, retain) NSString *urlString;

//--------------------------------------------------------------------------------------------------
- (id)initWithUrlString:(NSString *)urlString;

//--------------------------------------------------------------------------------------------------
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType;
- (void)webViewDidStartLoad:(UIWebView *)webView ;
- (void)webViewDidFinishLoad:(UIWebView *)webView;
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error;

- (void) initContent;
- (void) startScanner:(NSString *)scannerType textViewArray:(NSArray *)textViewArray returnKey:(NSString *)returnKey;
- (void) returnFromScanner:(NSString *) returnKey textViewArray:(NSArray *)textViewArray resultArray:(NSArray *)resultArray;
@end

