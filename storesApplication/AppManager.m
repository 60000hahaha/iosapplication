//
//  AppManager.h
//  storesApplication
//
//  Created by Vendor on 20/4/15.
//  Copyright (c) 2015 shblimted. All rights reserved.
//

//--------------------------------------------------------------------------------------------------
//  0    1         2         3         4         5         6         7         8         9
//  567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890

//--------------------------------------------------------------------------------------------------
//  0    1         2         3         4         5         6         7         8         9
//  567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890

#import "AFNetworking.h"
#import "AppManager.h"


//--------------------------------------------------------------------------------------------------
#define APPMANAGER_KEY_STATUS										@"status"
#define APPMANAGER_KEY_AUTHTOKEN									@"auth_key"
#define APPMANAGER_KEY_HOST											@"host"
#define APPMANAGER_KEY_APIKEY										@"api_key"

#define APPMANAGER_SECRET_KEY										@"But only love can say - try again or walk away. But I believe for you and me. The sun will shine one day. So I'll just play my part. And pray you'll have a change of heart. But I can't make you see it through. That's something only love can do."

#define APPMANAGER_API_HOST											@"http://iosapp.shb122.tk/api/"
#define APPMANAGER_API_LOGIN										@"login"
#define APPMANAGER_LOGIN_API_KEY                                    @"HA123HA"
#define APPMANAGER_API_WEBAPP                                       @"webapp"

//#define ORDERLISTVIEW_ICON_THUMBNAIL								@"photo_mask.png"
//#define ORDERLISTVIEW_ICON_DELIVERY_DATE							@"icon_date_delivery.png"
//==================================================================================================
@implementation AppManager

//--------------------------------------------------------------------------------------------------
static AppManager *sharedManager = nil;
static NSString *_authToken = nil;

#pragma mark - Static functions
//--------------------------------------------------------------------------------------------------
+ (AppManager *)sharedManager  {
    if (sharedManager != nil)  {return sharedManager;}
    
    sharedManager = [[AppManager alloc] init];
    return sharedManager;
}

//--------------------------------------------------------------------------------------------------
+ (void)releaseManager  {
    sharedManager = nil;
}

//--------------------------------------------------------------------------------------------------
+ (NSString *)getAppVersionString  {
    NSBundle *mainBundle = [NSBundle mainBundle];
    NSString *version = [mainBundle objectForInfoDictionaryKey:@"CFBundleVersion"];
    NSString *buildCount = [mainBundle objectForInfoDictionaryKey:@"BUILD_COUNT"];
    NSString *string = [NSString stringWithFormat:@"%@.%@", version, buildCount];
    return string;
}

//--------------------------------------------------------------------------------------------------
+ (NSString *)getSystemVersion  {return [[UIDevice currentDevice] systemVersion];}
+ (NSString *)getDeviceModel  {return [[UIDevice currentDevice] model];}
+ (NSString *)getSystemLanguage  {return [[NSLocale currentLocale] localeIdentifier];}

//--------------------------------------------------------------------------------------------------
+ (NSDictionary *)verifyUsername:(NSString *)username andPassword:(NSString *)password  {
    NSDictionary *dictionary;
    NSString *message;
    
    //  Trim white space
    username = [username stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    password = [password stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    //--------------------------------------------------------------------------------------------------
    //  Check length of username
    if ([username length] <= 3)  {
        message = NSLocalizedString(@"APPMANAGER_WRONG_USERNAME", nil);
        dictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                      message, APPMANAGER_KEY_MESSAGE,
                      [NSString stringWithFormat:@"%d", APPMANAGER_RESULT_USERNAME], APPMANAGER_KEY_RESULT,
                      nil];
        return dictionary;
    }
    
    //--------------------------------------------------------------------------------------------------
    //  Check length of password
    if ([password length] <= 3)  {
        message = NSLocalizedString(@"APPMANAGER_WRONG_PASSWORD", nil);
        dictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                      message, APPMANAGER_KEY_MESSAGE,
                      [NSString stringWithFormat:@"%d", APPMANAGER_RESULT_PASSWORD], APPMANAGER_KEY_RESULT,
                      nil];
        return dictionary;
    }
    
    //--------------------------------------------------------------------------------------------------
    //  Success!
    dictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                  @"Login success", APPMANAGER_KEY_MESSAGE,
                  [NSString stringWithFormat:@"%d", APPMANAGER_RESULT_SUCCESS], APPMANAGER_KEY_RESULT,
                  nil];
    return dictionary;
}

//--------------------------------------------------------------------------------------------------
+ (void)loginWithUsername:(NSString *)username andPassword:(NSString *)password  {
    
    NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
    [preferences synchronize];
    
    NSString *host = [preferences objectForKey:APPMANAGER_KEY_HOST];
    if (host == nil)  {host = APPMANAGER_API_HOST;}
    
    NSString *urlString = [host stringByAppendingString:APPMANAGER_API_LOGIN];
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                username, @"username",
                                password, @"password",
                                APPMANAGER_LOGIN_API_KEY, @"api_key",
                                nil];
    
    [self saveUsername:username];
    
    
//    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] init];

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
//    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager POST:urlString parameters:dictionary success:^(AFHTTPRequestOperation *operation, id responseObject)  {
        
        //  Connection done
        if (responseObject == nil)  {return;}
        
//        //--------------------------------------------------------------------------------------------------
//        //  Check status value
//        NSNumber *number = [responseObject objectForKey:APPMANAGER_KEY_STATUS];
//        if (number == nil)  {
//            NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:@"Connection error...", APPMANAGER_KEY_MESSAGE, nil];
//            [[NSNotificationCenter defaultCenter] postNotificationName:APPMANAGER_NOTIFICATION_LOGIN_FAILED object:nil userInfo:userInfo];
//            return;
//        }
        
        //  0 = HTTP_STATUS_OK
//        int status = [number intValue];
//        if (status != 200)  {
//            
//            //  Failed
//            [[NSNotificationCenter defaultCenter] postNotificationName:APPMANAGER_NOTIFICATION_LOGIN_FAILED object:nil userInfo:responseObject];
//            return;
//        }
        
        //--------------------------------------------------------------------------------------------------
        //  Save token and latest time stamp
        _authToken = [responseObject objectForKey:APPMANAGER_KEY_AUTHTOKEN];
        [preferences setObject:_authToken forKey:APPMANAGER_KEY_AUTHTOKEN];
        [preferences synchronize];
        
        NSString *selectedUrl = [preferences objectForKey:APPMANAGER_KEY_SELECTED_URL];
        if (selectedUrl != nil)  {
            NSDictionary *dictionary =[[NSDictionary alloc] initWithObjectsAndKeys:selectedUrl, APPMANAGER_KEY_SELECTED_URL, nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:APPMANAGER_NOTIFICATION_LOGIN_SUCCESS object:nil userInfo:dictionary];
        } else {

        //--------------------------------------------------------------------------------------------------
        //  Everything alright, get webapp links
            [self getWebappLinks];
        }
        //--------------------------------------------------------------------------------------------------
        //  Everything alright, parse update content
//        NSDictionary *resultDictionary = [responseObject objectForKey:APPMANAGER_KEY_RESULT];
        
        //  Update settings
        //  * Somehow, if key is not present in dictionary, value will become NSNull
//        NSString *host = [resultDictionary objectForKey:APPMANAGER_KEY_HOST];
//        if (host != nil && [host isEqual:[NSNull null]] == NO)  {
//            [preferences setObject:host forKey:APPMANAGER_KEY_HOST];
//        }
        //        DataManager *manager = [DataManager sharedManager];
        //        //  TODO: Update database
        //        NSArray *salesmanArray = [resultDictionary objectForKey:APPMANAGER_KEY_SALESMAN];
        //        if ([salesmanArray count] > 0)  {
        //            [manager updateSalesman:salesmanArray];
        //            //DONE
        //        }
        
//        [preferences synchronize];
        
        //--------------------------------------------------------------------------------------------------
        //  Login success
        
    }  failure:^(AFHTTPRequestOperation *operation, NSError *error)  {
        
        //--------------------------------------------------------------------------------------------------
        //  Error
        //        BZLog(@"### %@", [error localizedDescription]);
        //        if (error.code == NSURLErrorNetworkConnectionLost || error.code == NSURLErrorCannotConnectToHost)  {
        //
        //            //  No network, login locally
        ////            DataManager *manager = [DataManager sharedManager];
        //            NSDictionary *dictionary = [manager localLoginWithUsername:username andPassword:password];
        //            DATAMANAGER_STATUS status = [[dictionary objectForKey:DATAMANAGER_KEY_STATUS] intValue];
        //            if (status == DATAMANAGER_STATUS_WRONG_USERNAME || status == DATAMANAGER_STATUS_WRONG_PASSWORD)  {
        //
        //                NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:@"Login name or password is wrong...", APPMANAGER_KEY_MESSAGE, nil];
        //                [[NSNotificationCenter defaultCenter] postNotificationName:APPMANAGER_NOTIFICATION_LOGIN_FAILED object:nil userInfo:userInfo];
        //                return;
        //            }
        //
        //            //  Error other than invalid account
        //            if (status != DATAMANAGER_STATUS_OK)  {
        //
        //                NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:@"Unknown error...", APPMANAGER_KEY_MESSAGE, nil];
        //                [[NSNotificationCenter defaultCenter] postNotificationName:APPMANAGER_NOTIFICATION_LOGIN_FAILED object:nil userInfo:userInfo];
        //                return;
        //            }
        //
        //            //--------------------------------------------------------------------------------------------------
        //            //  Login success
        //            [[NSNotificationCenter defaultCenter] postNotificationName:APPMANAGER_NOTIFICATION_LOGIN_SUCCESS object:nil userInfo:dictionary];
        //            return;
        //        }
        
        //--------------------------------------------------------------------------------------------------
        //  Other non-handled error
        NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:error.localizedDescription, APPMANAGER_KEY_MESSAGE, nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:APPMANAGER_NOTIFICATION_LOGIN_FAILED object:nil userInfo:userInfo];
    }];
}

//--------------------------------------------------------------------------------------------------
+ (NSString *)getUsername  {
    NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
    NSString *string = [preferences objectForKey:APPMANAGER_KEY_USERNAME];
    
    //    NSString *username = [BZMaths decodeString2:string keyString:APPMANAGER_SECRET_KEY];
    return string;
}

//--------------------------------------------------------------------------------------------------
+ (void)saveUsername:(NSString *)string  {
    if (string == nil)  {return;}
    if ([string isEqual:[NSNull null]])  {return;}
    
    //    NSString *encodedUsername = [BZMaths encodeString2:string keyString:APPMANAGER_SECRET_KEY];
    
    NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
    [preferences setObject:string forKey:APPMANAGER_KEY_USERNAME];
    [preferences synchronize];
    
}

//--------------------------------------------------------------------------------------------------
+ (void)getWebappLinks  {
    NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
    [preferences synchronize];
    
    NSString *host = [preferences objectForKey:APPMANAGER_KEY_HOST];
    if (host == nil)  {host = APPMANAGER_API_HOST;}
    
    
    
    NSString *urlString = [host stringByAppendingString:APPMANAGER_API_WEBAPP];
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                _authToken, APPMANAGER_KEY_AUTHTOKEN,
                                nil];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager POST:urlString parameters:dictionary success:^(AFHTTPRequestOperation *operation, id responseObject)  {
        
        //  Connection done
        if (responseObject == nil)  {return;}
        
        //--------------------------------------------------------------------------------------------------
        //  Check status value
//        NSNumber *number = [responseObject objectForKey:APPMANAGER_KEY_STATUS];
//        if (number == nil)  {
//            NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:@"Connection error...", APPMANAGER_KEY_MESSAGE, nil];
//            [[NSNotificationCenter defaultCenter] postNotificationName:APPMANAGER_NOTIFICATION_GETWEBAPP_FAILED object:nil userInfo:userInfo];
//            return;
//        }
//        
//        //  0 = HTTP_STATUS_OK
//        int status = [number intValue];
//        if (status != 200)  {
//            
//            //  Failed
//            [[NSNotificationCenter defaultCenter] postNotificationName:APPMANAGER_NOTIFICATION_GETWEBAPP_FAILED object:nil userInfo:responseObject];
//            return;
//        }
//        
        //--------------------------------------------------------------------------------------------------
        //  Save token and latest time stamp
//        _authToken = [responseObject objectForKey:APPMANAGER_KEY_AUTHTOKEN];
        
        //--------------------------------------------------------------------------------------------------
        //  Everything alright, get webapp links
//        [self getWebappLinks];
        
        
        
        //--------------------------------------------------------------------------------------------------
        //  Everything alright, parse update content
        //        NSDictionary *resultDictionary = [responseObject objectForKey:APPMANAGER_KEY_RESULT];
        
        //  Update settings
        //  * Somehow, if key is not present in dictionary, value will become NSNull
        //        NSString *host = [resultDictionary objectForKey:APPMANAGER_KEY_HOST];
        //        if (host != nil && [host isEqual:[NSNull null]] == NO)  {
        //            [preferences setObject:host forKey:APPMANAGER_KEY_HOST];
        //        }
        //        DataManager *manager = [DataManager sharedManager];
        //        //  TODO: Update database
        NSDictionary *userInfo;
        NSArray *webappArray = [responseObject objectForKey:APPMANAGER_KEY_WEBAPPS];
        
        if ([webappArray count] > 0)  {
            userInfo = [NSDictionary dictionaryWithObjectsAndKeys:webappArray, APPMANAGER_KEY_WEBAPPS, nil];
        }
        
        //        [preferences synchronize];
        
        //--------------------------------------------------------------------------------------------------
        //  Login success
        [[NSNotificationCenter defaultCenter] postNotificationName:APPMANAGER_NOTIFICATION_GETWEBAPP_SUCCESS object:nil userInfo:userInfo];
        
    }  failure:^(AFHTTPRequestOperation *operation, NSError *error)  {
        
        //--------------------------------------------------------------------------------------------------
        //  Error
        //        BZLog(@"### %@", [error localizedDescription]);
        //        if (error.code == NSURLErrorNetworkConnectionLost || error.code == NSURLErrorCannotConnectToHost)  {
        //
        //            //  No network, login locally
        ////            DataManager *manager = [DataManager sharedManager];
        //            NSDictionary *dictionary = [manager localLoginWithUsername:username andPassword:password];
        //            DATAMANAGER_STATUS status = [[dictionary objectForKey:DATAMANAGER_KEY_STATUS] intValue];
        //            if (status == DATAMANAGER_STATUS_WRONG_USERNAME || status == DATAMANAGER_STATUS_WRONG_PASSWORD)  {
        //
        //                NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:@"Login name or password is wrong...", APPMANAGER_KEY_MESSAGE, nil];
        //                [[NSNotificationCenter defaultCenter] postNotificationName:APPMANAGER_NOTIFICATION_LOGIN_FAILED object:nil userInfo:userInfo];
        //                return;
        //            }
        //
        //            //  Error other than invalid account
        //            if (status != DATAMANAGER_STATUS_OK)  {
        //
        //                NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:@"Unknown error...", APPMANAGER_KEY_MESSAGE, nil];
        //                [[NSNotificationCenter defaultCenter] postNotificationName:APPMANAGER_NOTIFICATION_LOGIN_FAILED object:nil userInfo:userInfo];
        //                return;
        //            }
        //
        //            //--------------------------------------------------------------------------------------------------
        //            //  Login success
        //            [[NSNotificationCenter defaultCenter] postNotificationName:APPMANAGER_NOTIFICATION_LOGIN_SUCCESS object:nil userInfo:dictionary];
        //            return;
        //        }
        
        //--------------------------------------------------------------------------------------------------
        //  Other non-handled error
        NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:error.localizedDescription, APPMANAGER_KEY_MESSAGE, nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:APPMANAGER_NOTIFICATION_LOGIN_FAILED object:nil userInfo:userInfo];
    }];
}

#pragma mark - Init & dealloc functions
//--------------------------------------------------------------------------------------------------
- (id)init  {
    self = [super init];
    if (self == nil)  {return self;}
    
    return self;
}

//--------------------------------------------------------------------------------------------------
- (void)dealloc  {
}

#pragma mark - Custom functions

@end