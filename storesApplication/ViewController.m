//
//  ViewController.m
//  storesApplication
//
//  Created by Ng Yu Wing on 9/2/15.
//  Copyright (c) 2015 shblimted. All rights reserved.
//

//--------------------------------------------------------------------------------------------------
//  0    1         2         3         4         5         6         7         8         9
//  567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890

#import "AppManager.h"
#import "ViewController.h"
#import "WebViewController.h"
#import "LoginViewController.h"
#import "BarcodeScannerViewController.h"
#import "UrlSelectionViewController.h"

//--------------------------------------------------------------------------------------------------
#define VIEWCONTROLLER_WEBVIEW_TARGETURL							@"http://www.google.com"
#define VIEWCONTROLLER_ANIM_TRANSITION                              @"animTransition"

//--------------------------------------------------------------------------------------------------
@implementation ViewController

//--------------------------------------------------------------------------------------------------
@synthesize viewController = _viewController;
@synthesize webView = _webView;
@synthesize lastViewController = _lastViewController;

//--------------------------------------------------------------------------------------------------
- (void)viewDidLoad {
	[super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    //--------------------------------------------------------------------------------------------------
    //  set up notification centers
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(processNotification:) name:APPMANAGER_NOTIFICATION_LOGIN_SUCCESS object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(processNotification:) name:APPMANAGER_NOTIFICATION_LOGOUT object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(processNotification:) name:APPMANAGER_NOTIFICATION_SCANNER_STOP object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(processNotification:) name:APPMANAGER_NOTIFICATION_SCANNER_START object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(processNotification:) name:APPMANAGER_NOTIFICATION_WEBAPP_SELECTED object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(processNotification:) name:APPMANAGER_NOTIFICATION_GETWEBAPP_SUCCESS object:nil];
    
    LoginViewController *controller = [[LoginViewController alloc] init];
    [self performTransition:controller];
	
}

//--------------------------------------------------------------------------------------------------
- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

//--------------------------------------------------------------------------------------------------
-(void) processNotification:(NSNotification *) notification  {
    
    if ([[notification name] isEqualToString:APPMANAGER_NOTIFICATION_GETWEBAPP_SUCCESS])  {
        NSDictionary *dictionary = [notification userInfo];
        NSArray *array = [dictionary objectForKey:APPMANAGER_KEY_WEBAPPS];
        
        UrlSelectionViewController *controller = [[UrlSelectionViewController alloc] init];
        [controller setUrlArray:array];
        
        [self performTransition:controller];
    }
    
    if ([[notification name] isEqualToString:APPMANAGER_NOTIFICATION_LOGIN_SUCCESS] || [[notification name] isEqualToString:APPMANAGER_NOTIFICATION_WEBAPP_SELECTED])  {
        NSDictionary *dictionary = [notification userInfo];
        NSString *string = [dictionary objectForKey:APPMANAGER_KEY_SELECTED_URL];
        
//        NSLog(@"url = %@", string);
        WebViewController *controller = [[WebViewController alloc] initWithUrlString:string];
        
        [self performTransition:controller];
    }
    
    if ([[notification name] isEqualToString:APPMANAGER_NOTIFICATION_SCANNER_START])  {
        if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])  {
            NSDictionary *dictionary = [notification userInfo];
            NSString *type = [dictionary objectForKey:APPMANAGER_SCANNER_KEY_TYPE];
            NSArray *array = [dictionary objectForKey:APPMANAGER_SCANNER_KEY_TEXTVIEW_ARRAY];
            NSString *returnKey = [dictionary objectForKey:APPMANAGER_SCANNER_KEY_RETURN_KEY];
            
            BarcodeScannerViewController *controller = [[BarcodeScannerViewController alloc] init];
            [controller setScannerType:type];
            [controller setReturnKey:returnKey];
            [controller setTextViewArray:array];
            
            [self performTransition:controller];
        } else {
            NSLog(@"No Camera!");
            NSDictionary *dictionary = [notification userInfo];
            NSArray *array = [dictionary objectForKey:APPMANAGER_SCANNER_KEY_TEXTVIEW_ARRAY];
            NSString *returnKey = [dictionary objectForKey:APPMANAGER_SCANNER_KEY_RETURN_KEY];
            
            NSArray *resultArray = [[NSArray alloc] initWithObjects:
                                    @"24564875",
                                    nil];
            
            WebViewController *controller = (WebViewController *)self.viewController;
            [controller returnFromScanner:returnKey textViewArray:array resultArray:resultArray];
        }
    }
    
    if ([[notification name] isEqualToString:APPMANAGER_NOTIFICATION_SCANNER_STOP])  {
        NSDictionary *dictionary = [notification userInfo];
        NSArray *array = [dictionary objectForKey:APPMANAGER_SCANNER_KEY_TEXTVIEW_ARRAY];
        NSArray *resultArray = [dictionary objectForKey:APPMANAGER_SCANNER_KEY_RESULT_ARRAY];
        NSString *returnKey = [dictionary objectForKey:APPMANAGER_SCANNER_KEY_RETURN_KEY];
        
        WebViewController *controller = (WebViewController *)self.lastViewController;
        [controller returnFromScanner:returnKey textViewArray:array resultArray:resultArray];
        
        [self pushBackViewController:controller];
    }
    
    if ([[notification name] isEqualToString:APPMANAGER_NOTIFICATION_LOGOUT])  {
        LoginViewController *controller = [[LoginViewController alloc] init];
        
        [self performTransition:controller];
    }
    
}

//--------------------------------------------------------------------------------------------------
- (void) performTransition:(UIViewController *)controller  {
    CGFloat x, y, width, height;
    CGRect screenRect;
    screenRect = [[UIScreen mainScreen] bounds];
    
    x = 0;
    y = 0;
    width = screenRect.size.width;
    height = screenRect.size.height;
    
    [controller.view setFrame:CGRectMake(x, y, width, height)];
    [self.view addSubview:controller.view];
    [controller.view setAlpha:0.0f];
    
    _lastViewController = self.viewController;
    self.viewController = controller;
    
    //--------------------------------------------------------------------------------------------------
    //  Prepare for animation
    [UIView beginAnimations:VIEWCONTROLLER_ANIM_TRANSITION context:NULL];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.3f];
    [self.viewController.view setAlpha:1.0f];
    [UIView commitAnimations];
}

//--------------------------------------------------------------------------------------------------
- (void) pushBackViewController:(UIViewController *)controller  {
    [self.view bringSubviewToFront:controller.view];
    [controller.view setAlpha:0.0f];
    
    _lastViewController = self.viewController;
    self.viewController = controller;
    
    //--------------------------------------------------------------------------------------------------
    //  Prepare for animation
    [UIView beginAnimations:VIEWCONTROLLER_ANIM_TRANSITION context:NULL];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDuration:0.3f];
    [self.viewController.view setAlpha:1.0f];
    [UIView commitAnimations];
}

@end
