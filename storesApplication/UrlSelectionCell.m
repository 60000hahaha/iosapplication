//
//  UrlSelectionCell.m
//  PriceriteApp
//
//  Created by vendor on 26/5/15.
//  Copyright (c) 2015 shblimted. All rights reserved.
//

#import "UrlSelectionCell.h"
#import "AppManager.h"

//--------------------------------------------------------------------------------------------------
#define LOGINVIEWCONTROLLER_IMAGENAME_LOGO                          @"pricerite_logo.jpg"

#define LOGINVIEWCONTROLLER_TEXTFIELD_HEIGHT                        60

//--------------------------------------------------------------------------------------------------
@implementation UrlSelectionCell

@synthesize label = _label;

//--------------------------------------------------------------------------------------------------
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        // Helpers
        CGFloat x, y, width, height, margin;
        UILabel *label;
        CGRect screenRect;
        screenRect = [[UIScreen mainScreen] bounds];
        
        x = 0;
        y = 0;
        width = screenRect.size.width;
        height = self.frame.size.height;
        
        // Initialize Main Label
        label = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
        
        // Configure Main Label
        [label setTextAlignment:NSTextAlignmentCenter];
        [label setTextColor:[UIColor blackColor]];
        
        // Add Main Label to Content View
        [self.contentView addSubview:label];
        _label = label;
    }
    
    return self;
}
@end