//
//  UrlSelectionCell.h
//  PriceriteApp
//
//  Created by vendor on 26/5/15.
//  Copyright (c) 2015 shblimted. All rights reserved.
//

#import <UIKit/UIKit.h>

//--------------------------------------------------------------------------------------------------
@class AppManager;

//--------------------------------------------------------------------------------------------------
@interface UrlSelectionCell : UITableViewCell  {
    UILabel *_label;
    
    AppManager *_appManager;
}

@property (nonatomic, retain) UILabel *label;

//--------------------------------------------------------------------------------------------------
- (void)viewDidLoad;
//- (void)viewWillAppear:(BOOL)animated;
//- (void)viewWillDisappear:(BOOL)animated;

//--------------------------------------------------------------------------------------------------

@end

