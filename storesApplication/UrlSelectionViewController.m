//
//  UrlSelectionViewController.m
//  storesApplication
//
//  Created by vendor on 7/4/15.
//  Copyright (c) 2015 shblimted. All rights reserved.
//

//--------------------------------------------------------------------------------------------------
//  0    1         2         3         4         5         6         7         8         9
//  567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890

#import "UrlSelectionViewController.h"
#import "UrlSelectionCell.h"
#import "AppManager.h"

//--------------------------------------------------------------------------------------------------
#define URLSELECTIONVIEWCONTROLLER_CELL_IDENTIFIER                  @"url_selection_cell"
#define URLSELECTIONVIEWCONTROLLER_WEBAPPS_KEY_NAME                 @"name"
#define URLSELECTIONVIEWCONTROLLER_WEBAPPS_KEY_URL                  @"url"
#define HEADERVIEW_BACK_BUTTON_IMAGE                                @"back_button.png"
#define HEADERVIEW_HEIGHT                                           60

#define URLSELECTIONVIEWCONTROLLER_CELL_HEIGHT                      60

//--------------------------------------------------------------------------------------------------
@implementation UrlSelectionViewController

@synthesize urlArray = _urlArray;

//--------------------------------------------------------------------------------------------------
- (void) viewWillAppear:(BOOL)animated  {
    [super viewWillAppear:animated];
    
    CGFloat x, y, width, height, margin;
    UITableView *tableView;
    NSString *string;
    UILabel *label;
    UIImage *image;
    UIButton *button;
    
    margin = 10;
    //--------------------------------------------------------------------------------------------------
    //  set header label
    x = 0;
    y = 0;
    width = self.view.frame.size.width;
    height = HEADERVIEW_HEIGHT;
    
    string = @"Url Selection";
    
    label = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
    [label setBackgroundColor:[UIColor greenColor]];
    [label setText:string];
    [label setTextAlignment:NSTextAlignmentCenter];
    [self.view addSubview:label];
    
    _label = label;
    
    x = margin;
    y = margin;
    height = HEADERVIEW_HEIGHT - margin -margin;
    width = height;
    
    image = [UIImage imageNamed:HEADERVIEW_BACK_BUTTON_IMAGE];
    
    button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(x, y, width, height)];
    [button setImage:image forState:UIControlStateNormal];
    [button addTarget:self action:@selector(logout) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:button];
    //--------------------------------------------------------------------------------------------------
    x = 0;
    y = HEADERVIEW_HEIGHT;
    width = self.view.frame.size.width;
    height = self.view.frame.size.height - HEADERVIEW_HEIGHT;
    
    tableView = [[UITableView alloc] initWithFrame:CGRectMake(x, y, width, height)];
    [tableView registerClass:[UrlSelectionCell class] forCellReuseIdentifier:URLSELECTIONVIEWCONTROLLER_CELL_IDENTIFIER];
    [tableView setDelegate:self];
    [tableView setDataSource:self];
    
    [self.view addSubview:tableView];
    
}

#pragma mark - Table view data source

//--------------------------------------------------------------------------------------------------
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    // Return the number of sections.
    // 告訴tableView總共有多少個section需要顯示
    return 1;
}

//--------------------------------------------------------------------------------------------------
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    // Return the number of rows in the section.
    // 告訴tableView一個section裡要顯示多少行
    return [_urlArray count];
}

//--------------------------------------------------------------------------------------------------
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UrlSelectionCell *cell = [tableView dequeueReusableCellWithIdentifier:URLSELECTIONVIEWCONTROLLER_CELL_IDENTIFIER];
    
    if (cell == nil) {
        cell = [[UrlSelectionCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:URLSELECTIONVIEWCONTROLLER_CELL_IDENTIFIER];
    }
    
    // Configure the cell...
    NSDictionary *dictionary = [_urlArray objectAtIndex:indexPath.row];
    NSString *string = [dictionary objectForKey:URLSELECTIONVIEWCONTROLLER_WEBAPPS_KEY_NAME];
    [cell.label setText:string];
    
    return cell;
}

//--------------------------------------------------------------------------------------------------
-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 85.0;
}

//--------------------------------------------------------------------------------------------------
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *dictionary = [_urlArray objectAtIndex:indexPath.row];
    NSString *urlString = [dictionary objectForKey:URLSELECTIONVIEWCONTROLLER_WEBAPPS_KEY_URL];
    
    dictionary = [[NSDictionary alloc] initWithObjectsAndKeys:urlString, APPMANAGER_KEY_SELECTED_URL, nil];
    
    
    NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
    [preferences synchronize];
    [preferences setObject:urlString forKey:APPMANAGER_KEY_SELECTED_URL];
    [preferences synchronize];
        
    [[NSNotificationCenter defaultCenter] postNotificationName:APPMANAGER_NOTIFICATION_WEBAPP_SELECTED object:nil userInfo:dictionary];

//    NSLog(@"url string = %@", urlString);
}

//--------------------------------------------------------------------------------------------------
- (void)logout  {
    [[NSNotificationCenter defaultCenter] postNotificationName:APPMANAGER_NOTIFICATION_LOGOUT object:nil userInfo:nil];
}

@end