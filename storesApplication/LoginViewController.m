//
//  LoginViewController.m
//  storesApplication
//
//  Created by vendor on 8/3/15.
//  Copyright (c) 2015 shblimted. All rights reserved.
//

//--------------------------------------------------------------------------------------------------
//  0    1         2         3         4         5         6         7         8         9
//  567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890

#import "LoginViewController.h"
#import "AppManager.h"

//--------------------------------------------------------------------------------------------------
#define LOGINVIEWCONTROLLER_IMAGENAME_LOGO                          @"pricerite_logo.jpg"

#define LOGINVIEWCONTROLLER_TEXTFIELD_HEIGHT                        60

//--------------------------------------------------------------------------------------------------
typedef enum  {
    LOGINVIEW_TAG_UNDEFINED = 0,
    LOGINVIEW_TAG_LOGIN,
}  LOGINVIEW_TAG;

//--------------------------------------------------------------------------------------------------
@implementation LoginViewController

//--------------------------------------------------------------------------------------------------
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    _appManager = [AppManager sharedManager];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    CGFloat x, y, width, height, margin;
    UIImage *image;
    UIImageView *imageView;
    UITextField *textField;
    UILabel *label;
    NSString *string;
    UIButton *button;
    
    margin = 10;
    
    //--------------------------------------------------------------------------------------------------
    image = [UIImage imageNamed:LOGINVIEWCONTROLLER_IMAGENAME_LOGO];
    
    x = (self.view.frame.size.width - image.size.width)*0.5f;
    y = margin *3;
    width = image.size.width;
    height = image.size.height;
    
    imageView = [[UIImageView alloc] initWithImage:image];
    [imageView setFrame:CGRectMake(x, y, width, height)];
    
    [self.view addSubview:imageView];
    
    //--------------------------------------------------------------------------------------------------
    x = margin;
    y += height +margin + margin;
    width = self.view.frame.size.width -margin -margin;
    height = LOGINVIEWCONTROLLER_TEXTFIELD_HEIGHT;
    
    string = NSLocalizedString(@"LOGINVIEW_USERNAME", nil);
    
    label = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
    
    [label setText:string];
    [label setTextAlignment:NSTextAlignmentCenter];
    
    [self.view addSubview:label];
    
    x = margin;
    y += height +margin + margin;
    width = self.view.frame.size.width -margin -margin;
    height = LOGINVIEWCONTROLLER_TEXTFIELD_HEIGHT;
    
    textField = [[UITextField alloc] initWithFrame:CGRectMake(x, y, width, height)];
    [textField setPlaceholder:string];
    [textField setTextAlignment:NSTextAlignmentCenter];
    [textField setAutocapitalizationType:UITextAutocapitalizationTypeNone];
    [textField setAutocorrectionType:UITextAutocapitalizationTypeNone];
    [self.view addSubview:textField];
    _usernameField = textField;
    
    //--------------------------------------------------------------------------------------------------
    x = margin;
    y += height +margin + margin;
    width = self.view.frame.size.width -margin -margin;
    height = LOGINVIEWCONTROLLER_TEXTFIELD_HEIGHT;
    
    string = NSLocalizedString(@"LOGINVIEW_PASSWORD", nil);
    
    label = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
    
    [label setText:string];
    [label setTextAlignment:NSTextAlignmentCenter];
    
    [self.view addSubview:label];
    
    x = margin;
    y += height +margin + margin;
    width = self.view.frame.size.width -margin -margin;
    height = LOGINVIEWCONTROLLER_TEXTFIELD_HEIGHT;
    
    textField = [[UITextField alloc] initWithFrame:CGRectMake(x, y, width, height)];
    [textField setPlaceholder:string];
    [textField setSecureTextEntry:YES];
    [textField setTextAlignment:NSTextAlignmentCenter];
    [self.view addSubview:textField];
    _passwordField = textField;
    
    //--------------------------------------------------------------------------------------------------
    y += height+margin+margin;
    
    string = NSLocalizedString(@"LOGINVIEW_LOGIN", nil);
    
    button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button setTag:LOGINVIEW_TAG_LOGIN];
    [button setFrame:CGRectMake(x, y, width, height)];
    [button setTitle:string forState:UIControlStateNormal];
    [button addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
    
    //--------------------------------------------------------------------------------------------------
    NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
    string = [preferences objectForKey:APPMANAGER_KEY_USERNAME];
    if (string != nil)
    [_usernameField setText:string];

}

//--------------------------------------------------------------------------------------------------
- (void)viewWillDisappear:(BOOL)animated  {
    NSArray *array =[self.view subviews];
    for (UIView *subview in array)  {
        [subview removeFromSuperview];
    }
}

//--------------------------------------------------------------------------------------------------
- (void)buttonClicked:(UIButton *)button  {
    if (button.tag == LOGINVIEW_TAG_LOGIN)  {
        NSString *username = _usernameField.text;
        NSString *password = _passwordField.text;
        
        //  Simply check username and password first
        NSDictionary *dictionary = [AppManager verifyUsername:username andPassword:password];
        int result = [[dictionary objectForKey:APPMANAGER_KEY_RESULT] intValue];
        if (result < APPMANAGER_RESULT_SUCCESS)  {
            
            //  Show error message
            NSString *errorString = [dictionary objectForKey:APPMANAGER_KEY_MESSAGE];
        
            //  Get app name
            NSDictionary *dictionary = [[NSBundle mainBundle] localizedInfoDictionary];
            NSString *title = [dictionary objectForKey:@"CFBundleDisplayName"];
            
            NSString *ok = NSLocalizedString(@"BUTTON_OK", nil);
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:errorString delegate:nil cancelButtonTitle:ok otherButtonTitles:nil];
            [alertView show];
            return;
        }
        [_usernameField resignFirstResponder];
        [_passwordField resignFirstResponder];
        
        [AppManager loginWithUsername:username andPassword:password];

    }
}

@end