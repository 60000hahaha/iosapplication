//
//  AppManager.h
//  storesApplication
//
//  Created by Vendor on 20/4/15.
//  Copyright (c) 2015 shblimted. All rights reserved.
//

//--------------------------------------------------------------------------------------------------
//  0    1         2         3         4         5         6         7         8         9
//  567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890

#import <Foundation/Foundation.h>

//--------------------------------------------------------------------------------------------------
#define APPMANAGER_KEY_MESSAGE										@"message"
#define APPMANAGER_KEY_RESULT										@"result"
#define APPMANAGER_KEY_USERNAME										@"username"
#define APPMANAGER_KEY_PASSWORD										@"password"
#define APPMANAGER_KEY_WEBAPPS										@"webapps"
#define APPMANAGER_KEY_SELECTED_URL                                 @"selectedUrl"

#define APPMANAGER_SCANNER_KEY_TYPE                                 @"scannerType"
#define APPMANAGER_SCANNER_KEY_TEXTVIEW_ARRAY                       @"textviewArray"
#define APPMANAGER_SCANNER_KEY_RESULT_ARRAY                         @"resultArray"
#define APPMANAGER_SCANNER_KEY_RETURN_KEY                           @"returnKey"

#define APPMANAGER_NOTIFICATION_LOGIN_SUCCESS						@"loginSuccess"
#define APPMANAGER_NOTIFICATION_LOGIN_FAILED						@"loginFailed"
#define APPMANAGER_NOTIFICATION_LOGOUT                              @"logout"
#define APPMANAGER_NOTIFICATION_GETWEBAPP_SUCCESS					@"getWebappSuccess"
#define APPMANAGER_NOTIFICATION_GETWEBAPP_FAILED					@"getWebappFailed"
#define APPMANAGER_NOTIFICATION_WEBAPP_SELECTED                     @"webAppSelected"
#define APPMANAGER_NOTIFICATION_SCANNER_START                       @"scannerStart"
#define APPMANAGER_NOTIFICATION_SCANNER_STOP                        @"scannerStop"

//--------------------------------------------------------------------------------------------------
typedef enum  {
    APPMANAGER_RESULT_USERNAME = -2,
    APPMANAGER_RESULT_PASSWORD = -1,
    APPMANAGER_RESULT_UNDEFINED = 0,
    APPMANAGER_RESULT_SUCCESS,
}  APPMANAGER_RESULT;

//==================================================================================================
@interface AppManager : NSObject  {
}

//--------------------------------------------------------------------------------------------------
#pragma mark - Static functions
+ (AppManager *)sharedManager;
+ (void)releaseManager;

+ (NSString *)getAppVersionString;
+ (NSString *)getSystemVersion;
+ (NSString *)getDeviceModel;
+ (NSString *)getSystemLanguage;

+ (NSDictionary *)verifyUsername:(NSString *)username andPassword:(NSString *)password;
+ (void)loginWithUsername:(NSString *)username andPassword:(NSString *)password;

+ (NSString *)getUsername;
+ (void)saveUsername:(NSString *)string;

#pragma mark - Init & dealloc functions
- (id)init;
- (void)dealloc;

#pragma mark - Custom functions
+ (void)getWebappLinks;
@end