//
//  BarcodeScannerViewController.h
//  storesApplication
//
//  Created by vendor on 8/3/15.
//  Copyright (c) 2015 shblimted. All rights reserved.
//

//--------------------------------------------------------------------------------------------------
//  0    1         2         3         4         5         6         7         8         9
//  567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890

#import <AVFoundation/AVFoundation.h>
#import <UIKit/UIKit.h>

//--------------------------------------------------------------------------------------------------
@interface BarcodeScannerViewController : UIViewController  {
    AVCaptureSession *_session;
    AVCaptureDevice *_device;
    AVCaptureDeviceInput *_input;
    AVCaptureMetadataOutput *_output;
    AVCaptureVideoPreviewLayer *_prevLayer;
    
    UIView *_highlightView;
    UILabel *_label;
    
    NSString *_scannerType;
    NSArray *_textViewArray;
    NSString *_returnKey;
    NSMutableArray *_resultArray;
    
    int _capturedCount;
}

//--------------------------------------------------------------------------------------------------
@property (nonatomic, retain) NSString *scannerType;
@property (nonatomic, retain) NSArray *textViewArray;
@property (nonatomic, retain) NSString *returnKey;

//--------------------------------------------------------------------------------------------------
//- (id)initWithFrame:(CGRect)frame;

@end

