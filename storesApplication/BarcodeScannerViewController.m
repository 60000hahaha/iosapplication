//
//  BarscannerViewController.m
//  storesApplication
//
//  Created by vendor on 21/2/15.
//  Copyright (c) 2015 shblimted. All rights reserved.
//

//--------------------------------------------------------------------------------------------------
//  0    1         2         3         4         5         6         7         8         9
//  567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890

#import "BarcodeScannerViewController.h"
#import "AppManager.h"

//--------------------------------------------------------------------------------------------------
#define HEADERVIEW_HEIGHT                                           60

#define HEADERVIEW_BACK_BUTTON_IMAGE                                @"back_button.png"

//--------------------------------------------------------------------------------------------------
@implementation BarcodeScannerViewController

//--------------------------------------------------------------------------------------------------
@synthesize scannerType = _scannerType;
@synthesize textViewArray = _textViewArray;
@synthesize returnKey = _returnKey;

//--------------------------------------------------------------------------------------------------
- (void)viewWillAppear:(BOOL)animated  {
    [super viewWillAppear:animated];
    _capturedCount = 0;
    
    CGFloat x, y, width, height, margin;
//    UIWebView *webView;
    //    UIView *view;
    UILabel *label;
    NSString *string;
    UIButton *button;
    UIImage *image;
    
    margin = 10;
    //--------------------------------------------------------------------------------------------------
    //  set header label
    x = 0;
    y = 0;
    width = self.view.frame.size.width;
    height = HEADERVIEW_HEIGHT;
    
    string = @"Barcode Scanner";
    
    label = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
    [label setBackgroundColor:[UIColor greenColor]];
    [label setText:string];
    [label setTextAlignment:NSTextAlignmentCenter];
    [self.view addSubview:label];
    
    _label = label;
    
    x = margin;
    y = margin;
    height = HEADERVIEW_HEIGHT - margin -margin;
    width = height;
    
    image = [UIImage imageNamed:HEADERVIEW_BACK_BUTTON_IMAGE];
    
    button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(x, y, width, height)];
    [button setImage:image forState:UIControlStateNormal];
    [button addTarget:self action:@selector(stopBarCodeScanner) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:button];
    
    //--------------------------------------------------------------------------------------------------
    //  set highlight view
    
    _highlightView = [[UIView alloc] init];
    [_highlightView setAutoresizingMask:UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin|UIViewAutoresizingFlexibleBottomMargin];
    [_highlightView.layer setBorderColor:[UIColor greenColor].CGColor];
    [_highlightView.layer setBorderWidth:3];
    [self.view addSubview:_highlightView];
    
    //--------------------------------------------------------------------------------------------------
    //  set capture section
    
    _session = [[AVCaptureSession alloc] init];
    _device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    NSError *error = nil;
    
    _input = [AVCaptureDeviceInput deviceInputWithDevice:_device error:&error];
    if (_input) {
        [_session addInput:_input];
    } else {
        NSLog(@"Error: %@", error);
    }
    
    _output = [[AVCaptureMetadataOutput alloc] init];
    [_output setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
    [_session addOutput:_output];
    
    _output.metadataObjectTypes = [_output availableMetadataObjectTypes];
    
    _prevLayer = [AVCaptureVideoPreviewLayer layerWithSession:_session];
    _prevLayer.frame = self.view.bounds;
    _prevLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [self.view.layer addSublayer:_prevLayer];
    
    [_session startRunning];
    
    [self.view bringSubviewToFront:_highlightView];
    [self.view bringSubviewToFront:_label];
    [self.view bringSubviewToFront:button];

}

//--------------------------------------------------------------------------------------------------
- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection  {
    CGRect highlightViewRect = CGRectZero;
    AVMetadataMachineReadableCodeObject *barCodeObject;
    NSString *detectionString = nil;
    NSArray *barCodeTypes = @[AVMetadataObjectTypeUPCECode, AVMetadataObjectTypeCode39Code, AVMetadataObjectTypeCode39Mod43Code,
                              AVMetadataObjectTypeEAN13Code, AVMetadataObjectTypeEAN8Code, AVMetadataObjectTypeCode93Code, AVMetadataObjectTypeCode128Code,
                              AVMetadataObjectTypePDF417Code, AVMetadataObjectTypeQRCode, AVMetadataObjectTypeAztecCode];
    
    for (AVMetadataObject *metadata in metadataObjects) {
        for (NSString *type in barCodeTypes) {
            if ([metadata.type isEqualToString:type])  {
                barCodeObject = (AVMetadataMachineReadableCodeObject *)[_prevLayer transformedMetadataObjectForMetadataObject:(AVMetadataMachineReadableCodeObject *)metadata];
                highlightViewRect = barCodeObject.bounds;
                detectionString = [(AVMetadataMachineReadableCodeObject *)metadata stringValue];
                break;
            }
        }
        
        if (detectionString != nil)  {
            _highlightView.frame = highlightViewRect;
            if (_resultArray.count > 0)
            if ([detectionString isEqualToString:[_resultArray objectAtIndex:(_resultArray.count-1)]]) break;
            [self barcodeDetected:detectionString];
            break;
        }
    }
    
}

//--------------------------------------------------------------------------------------------------
- (void)barcodeDetected:(NSString *)string  {
//    _label.text = string;
    _capturedCount++;
    if (_resultArray == nil)  {
        _resultArray = [[NSMutableArray alloc] init];
    }
    
    [_resultArray addObject:string];
    if (_capturedCount >= _textViewArray.count)  {
        //  stop barcodeScanner
        [self stopBarCodeScanner];
    }
}

//--------------------------------------------------------------------------------------------------
- (void)stopBarCodeScanner  {
    //  collect the data and snd notification
    [_session stopRunning];
    
    NSDictionary *dictionary = [[NSDictionary alloc] initWithObjectsAndKeys:
                                _resultArray, APPMANAGER_SCANNER_KEY_RESULT_ARRAY,
                                _textViewArray, APPMANAGER_SCANNER_KEY_TEXTVIEW_ARRAY,
                                _returnKey, APPMANAGER_SCANNER_KEY_RETURN_KEY,
                                nil];
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:APPMANAGER_NOTIFICATION_SCANNER_STOP object:nil userInfo:dictionary];
}



@end
