//
//  UrlSelectionViewController.h
//  storesApplication
//
//  Created by vendor on 21/2/15.
//  Copyright (c) 2015 shblimted. All rights reserved.
//

//--------------------------------------------------------------------------------------------------
//  0    1         2         3         4         5         6         7         8         9
//  567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890

#import <UIKit/UIKit.h>

//--------------------------------------------------------------------------------------------------
@class AppManager;

//--------------------------------------------------------------------------------------------------
@interface UrlSelectionViewController : UIViewController  {
    NSArray *_urlArray;
    UILabel *_label;
    UITableView *_tableView;
    
    AppManager *_appManager;
}

@property (nonatomic, retain) NSArray *urlArray;

//--------------------------------------------------------------------------------------------------
//- (void)viewDidLoad;
- (void)viewWillAppear:(BOOL)animated;
- (void)viewWillDisappear:(BOOL)animated;

//--------------------------------------------------------------------------------------------------

@end

