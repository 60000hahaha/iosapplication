//
//  AppDelegate.h
//  storesApplication
//
//  Created by Ng Yu Wing on 9/2/15.
//  Copyright (c) 2015 shblimted. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>  {
    UIViewController *_viewController;
    UIWindow *_window;
}

//--------------------------------------------------------------------------------------------------
@property (strong, nonatomic) UIViewController *viewController;
@property (strong, nonatomic) UIWindow *window;


@end

