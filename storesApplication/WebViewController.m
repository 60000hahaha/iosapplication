//
//  WebViewController.m
//  storesApplication
//
//  Created by vendor on 21/2/15.
//  Copyright (c) 2015 shblimted. All rights reserved.
//

//--------------------------------------------------------------------------------------------------
//  0    1         2         3         4         5         6         7         8         9
//  567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890

#import "WebViewController.h"
#import "AppManager.h"

#define WEBVIEWCONTROLLER_TARGETURL                                 @"http://shb122.tk/iosApp/"
#define HEADERVIEW_HEIGHT                                           60
#define HEADERVIEW_BACK_BUTTON_IMAGE                                @"back_button.png"

//--------------------------------------------------------------------------------------------------
@implementation WebViewController

//--------------------------------------------------------------------------------------------------
@synthesize webView = _webView;
@synthesize urlString = _urlString;

//--------------------------------------------------------------------------------------------------
- (id)init  {
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    CGFloat x, y, width, height, margin;
    UIWebView *webView;
    //    UIView *view;
    UILabel *label;
    NSString *string;
    UIButton *button;
    UIImage *image;
    
    margin = 10;
    
    //--------------------------------------------------------------------------------------------------
    //  set header label
    x = 0;
    y = 0;
    width = self.view.frame.size.width;
    height = HEADERVIEW_HEIGHT;
    
    string = @"Webview";
    
    label = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
    [label setBackgroundColor:[UIColor greenColor]];
    [label setText:string];
    [label setTextAlignment:NSTextAlignmentCenter];
    [self.view addSubview:label];
    
    x = margin;
    y = margin;
    height = HEADERVIEW_HEIGHT - margin -margin;
    width = height;
    
    image = [UIImage imageNamed:HEADERVIEW_BACK_BUTTON_IMAGE];
    
    button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(x, y, width, height)];
    [button setImage:image forState:UIControlStateNormal];
    [button addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:button];
    
    //--------------------------------------------------------------------------------------------------
    x = 0;
    y = HEADERVIEW_HEIGHT;
    width = self.view.frame.size.width;
    height = self.view.frame.size.height-HEADERVIEW_HEIGHT;
    
    webView = [[UIWebView alloc] initWithFrame:CGRectMake(x, y, width, height)];
    [webView setDelegate:self];
    [webView setBackgroundColor:[UIColor clearColor]];
    [webView setOpaque:NO];
    [self.view addSubview:webView];
    
    self.webView = webView;
    
    //--------------------------------------------------------------------------------------------------
    //  Read content
    [self initContent];
    return self;
}

//--------------------------------------------------------------------------------------------------
- (id)initWithUrlString:(NSString *)urlString  {
    [self.view setBackgroundColor:[UIColor whiteColor]];
    _urlString = urlString;
    
    CGFloat x, y, width, height, margin;
    UIWebView *webView;
    //    UIView *view;
    UILabel *label;
    NSString *string;
    UIButton *button;
    UIImage *image;
    
    margin = 10;
    
    //--------------------------------------------------------------------------------------------------
    //  set header label
    x = 0;
    y = 0;
    width = self.view.frame.size.width;
    height = HEADERVIEW_HEIGHT;
    
    string = @"Webview";
    
    label = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
    [label setBackgroundColor:[UIColor greenColor]];
    [label setText:string];
    [label setTextAlignment:NSTextAlignmentCenter];
    [self.view addSubview:label];
    
    x = margin;
    y = margin;
    height = HEADERVIEW_HEIGHT - margin -margin;
    width = height;
    
    image = [UIImage imageNamed:HEADERVIEW_BACK_BUTTON_IMAGE];
    
    button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(x, y, width, height)];
    [button setImage:image forState:UIControlStateNormal];
    [button addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:button];
    
    //--------------------------------------------------------------------------------------------------
    x = 0;
    y = HEADERVIEW_HEIGHT;
    width = self.view.frame.size.width;
    height = self.view.frame.size.height-HEADERVIEW_HEIGHT;
    
    webView = [[UIWebView alloc] initWithFrame:CGRectMake(x, y, width, height)];
    [webView setDelegate:self];
    [webView setBackgroundColor:[UIColor clearColor]];
    [webView setOpaque:NO];
    [self.view addSubview:webView];
    
    self.webView = webView;
    
    
    //--------------------------------------------------------------------------------------------------
    //  Read content
    [self initContent];
    return self;
}

//--------------------------------------------------------------------------------------------------
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType  {
    NSLog(@"Loading URL :%@",request.URL.absoluteString);
    NSString *requestString = [[request.URL.absoluteString componentsSeparatedByString:@"/"] lastObject];
    NSArray *array = [requestString componentsSeparatedByString:@"?"];
    
    if (array.count <= 1) {
        return YES;
    }
    
    NSString *string = [array objectAtIndex:0];
    if ([string isEqualToString:WEBVIEW_SINGLE_SCANNER])  {
        array = [[array objectAtIndex:1] componentsSeparatedByString:@"&"];
        
        if ([array count] == 1)  {
            //  not auto submit
            string = [array objectAtIndex:0];
            array = [[[string componentsSeparatedByString:@"="] objectAtIndex:1] componentsSeparatedByString:@","];
            
            [self startScanner:WEBVIEW_SINGLE_SCANNER textViewArray:array returnKey:nil];
        } else {
            // auto submit
            string = [array objectAtIndex:0];
            NSArray *textViewArray = [[[string componentsSeparatedByString:@"="] objectAtIndex:1] componentsSeparatedByString:@","];
            string = [[[array objectAtIndex:1] componentsSeparatedByString:@"="] objectAtIndex:1];
            
            [self startScanner:WEBVIEW_SINGLE_SCANNER textViewArray:textViewArray returnKey:string];
        }
        return NO;
    } else if ([string isEqualToString: WEBVIEW_MULTIPLE_SCANNER])  {
        array = [[array objectAtIndex:1] componentsSeparatedByString:@"&"];
        
        if ([array count] == 1)  {
            //  not auto submit
            string = [array objectAtIndex:0];
            array = [[[string componentsSeparatedByString:@"="] objectAtIndex:1] componentsSeparatedByString:@","];
            
            [self startScanner:WEBVIEW_MULTIPLE_SCANNER textViewArray:array returnKey:nil];
        } else {
            // auto submit
            string = [array objectAtIndex:0];
            NSArray *textViewArray = [[[string componentsSeparatedByString:@"="] objectAtIndex:1] componentsSeparatedByString:@","];
            string = [[[array objectAtIndex:1] componentsSeparatedByString:@"="] objectAtIndex:1];
            
            [self startScanner:WEBVIEW_MULTIPLE_SCANNER textViewArray:textViewArray returnKey:string];
        }
        return NO;
    }
    
    //
    
    //return FALSE; //to stop loading
    return YES;
}

//--------------------------------------------------------------------------------------------------
- (void)webViewDidStartLoad:(UIWebView *)webView  {
    NSLog(@"web view started");
}

//--------------------------------------------------------------------------------------------------
- (void)webViewDidFinishLoad:(UIWebView *)webView  {
    
}

//--------------------------------------------------------------------------------------------------
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error  {
    NSLog(@"Failed to load with error :%@",[error debugDescription]);
    
}

//--------------------------------------------------------------------------------------------------
- (void) initContent  {
//    NSString *urlString = WEBVIEWCONTROLLER_TARGETURL;
    if (_urlString == nil)  {
        _urlString = WEBVIEWCONTROLLER_TARGETURL;
    }
    
    NSURL *url = [[NSURL alloc] initWithString:_urlString];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:urlRequest];
    
}

//--------------------------------------------------------------------------------------------------
- (void) buttonClicked:(UIButton *)button  {
    [AppManager getWebappLinks];
}

//--------------------------------------------------------------------------------------------------
- (void) startScanner:(NSString *)scannerType textViewArray:(NSArray *)textViewArray returnKey:(NSString *)returnKey   {
    NSDictionary *dictionary = [[NSDictionary alloc] initWithObjectsAndKeys:
                                scannerType, APPMANAGER_SCANNER_KEY_TYPE,
                                textViewArray, APPMANAGER_SCANNER_KEY_TEXTVIEW_ARRAY,
                                returnKey, APPMANAGER_SCANNER_KEY_RETURN_KEY,
                                nil];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:APPMANAGER_NOTIFICATION_SCANNER_START object:nil userInfo:dictionary];

}

//--------------------------------------------------------------------------------------------------
- (void) returnFromScanner:(NSString *)returnKey textViewArray:(NSArray *)textViewArray resultArray:(NSArray *)resultArray  {
    NSString *string;
    if (resultArray == nil || resultArray.count == 0)  {
        return;
    }
    for (int i =0; i < resultArray.count; i++)  {
        string = [NSString stringWithFormat:@"document.getElementById('%@').value = %@", [textViewArray objectAtIndex:i], [resultArray objectAtIndex:i]];
        [_webView stringByEvaluatingJavaScriptFromString:string];
    }
    
    if (returnKey != nil)  {
        string = [NSString stringWithFormat:@"document.getElementById('%@').click();", returnKey];
        [self performSelector:@selector(clickSearchKey:) withObject:string afterDelay:0.5f];
//        [_webView stringByEvaluatingJavaScriptFromString:string];
    }
}

//--------------------------------------------------------------------------------------------------
- (void) clickSearchKey:(NSString *)string  {
    [_webView stringByEvaluatingJavaScriptFromString:string];
}

@end
