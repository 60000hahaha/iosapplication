//
//  ViewController.h
//  storesApplication
//
//  Created by Ng Yu Wing on 9/2/15.
//  Copyright (c) 2015 shblimted. All rights reserved.
//

//--------------------------------------------------------------------------------------------------
//  0    1         2         3         4         5         6         7         8         9
//  567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890

#import <UIKit/UIKit.h>

@class AppManager;

//--------------------------------------------------------------------------------------------------
#define VIEWCONTROLLER_NOTIFICATION_LOGIN_SUCCESS                   @"login_success"
#define VIEWCONTROLLER_NOTIFICATION_LOGIN_FAIL                      @"login_fail"

#define VIEWCONTROLLER_NOTIFICATION_START_BARCODE_SCANNING          @"start_barcode_scanning"
#define VIEWCONTROLLER_NOTIFICATION_END_BARCODE_SCANNING            @"stop_barcode_scanning"

#define VIEWCONTROLLER_NOTIFICATION_SELECT_URL                      @"select_url"

//--------------------------------------------------------------------------------------------------
@interface ViewController : UIViewController <UIWebViewDelegate>  {
	UIViewController *_viewController;
    UIViewController *_lastViewController;
	UIWebView *_webView;
    
    AppManager *_appManager;
}

//--------------------------------------------------------------------------------------------------
@property (nonatomic, retain) UIViewController *viewController;
@property (nonatomic, retain) UIViewController *lastViewController;
@property (nonatomic, retain) UIWebView *webView;



@end

